module.exports = {
    // Мы хотим расширять конфиг от AirBnb (Base версия -- это версия без ReactJS)
    extends: ['airbnb-base'],
    
    // Мы пишем код на ES2019
    // А ещё используем Babel
    parserOptions: {
        ecmaVersion: 10,
        sourceType: 'module',
        parser: 'babel-eslint',
    },

    // Мы пишем код для браузера
    // Так ESLintбудет знать, о том, что у нас есть доступные в браузере объекты, функции и т.д.
    env: {
        browser: true,
        es6: true
    },

    // Переопределяем некоторые правила по своему усмотрению
    rules: {
        // Для экспериментов. 
        // Точка с запятой. Попробуйте 'never' вместо 'always'
        'semi': ['error', 'always'],
        // Фигурные скобки. Попробуйте 'allman' вместо 1tbs
        'brace-style': ['error', '1tbs'], 
 
        // Я считаю, что Default Export хуже для рефакторина и поддержки IDE
        // See https://basarat.gitbooks.io/typescript/docs/tips/defaultIsBad.html
        // See also https://blog.neufund.org/why-we-have-banned-default-exports-and-you-should-do-the-same-d51fdc2cf2ad
        'import/prefer-default-export': 'off',

        // Писать i++ -- это нормально
        'no-plusplus': 'off',
        
        // Использовать обычные for-in и for-of -- это нормально, необязательно использовать функциональный стиль
        'no-restricted-syntax': 'off',
        'guard-for-in': 'off',

        // Использование alert и confirm -- это, конечно, плохо, но для примера подойдут
        'no-restricted-globals': 'off',
        'no-alert': 'off'
    }
}