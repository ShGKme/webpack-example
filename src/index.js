// Polyfills
import 'core-js/stable'
import 'regenerator-runtime/runtime'

// jQuery (не требуется)
// import $ from 'jquery';

// Bootstrap (не требуется)
// import 'bootstrap/js/dist';

// Наши стили
import './scss/styles.scss'

// Объект Todos
import { ToDos } from './ToDos'

// Запуск
ToDos.init()
