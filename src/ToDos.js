export const ToDos = {
  loadingElement: document.getElementById('loading'),
  sectionContainer: document.getElementById('section-container'),
  errorElement: document.getElementById('error'),
  filterElement: document.getElementById('filter'),
  todoList: document.getElementById('todo-list'),

  async init()
  {
    try
    {
      const todos = await this.fetchTodos()
      this.fillTodosList(todos)
      this.filterElement.addEventListener('input', this.filterInputed)
    }
    catch (e)
    {
      this.showError(e.message)
    }
  },

  setLoading(loading)
  {
    this.loadingElement.style.display = loading ? '' : 'none'
  },

  showError(error)
  {
    this.errorElement.textContent = error
    this.errorElement.style.display = ''
  },

  async fetchTodos()
  {
    this.setLoading(true)
    const todosResponse = await fetch('https://jsonplaceholder.typicode.com/todos')
    this.setLoading(false)
    if (!todosResponse.ok)
    {
      throw new Error('Не удалось получить задачи... ')
    }
    return todosResponse.json()
  },

  fillTodosList(todos)
  {
    for (const todo of todos)
    {
      const todoItem = document.createElement('li')
      // List Item (ToDo)
      todoItem.classList.add('list-group-item')
      todoItem.classList.add('list-group-item-action')
      todoItem.classList.add('todo-item')
      todoItem.setAttribute('data-id', todo.id)
      // Todo Title
      const titleEl = document.createElement('span')
      titleEl.textContent = `!${todo.title}`
      if (todo.completed)
      {
        titleEl.classList.add('completed-todo')
      }

      todoItem.appendChild(titleEl)
      // Remove button
      const button = document.createElement('button')
      button.classList.add('btn')
      button.classList.add('btn-danger')
      button.classList.add('btn-sm')
      button.classList.add('btn-remove-todo')
      button.classList.add('ml-auto')
      button.textContent = 'Удалить'
      button.addEventListener('click', () =>
      {
        this.removeTodo(todo.id)
      })
      todoItem.appendChild(button)
      // Add todo to list
      this.todoList.appendChild(todoItem)
    }
  },

  filterInputed()
  {
    const filterText = document.getElementById('filter').value
    const todos = document.querySelectorAll('#section-container li')
    for (const todo of todos)
    {
      const todoTitle = todo.querySelector('span').textContent
      if (todoTitle.indexOf(filterText) !== -1)
      {
        todo.style.display = ''
      }
      else
      {
        todo.style.display = 'none'
      }
    }
  },

  async removeTodo(id)
  {
    if (!confirm('Вы уверены?'))
    {
      return
    }

    try
    {
      this.setLoading(true)
      const res = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, { method: 'delete' })
      this.setLoading(false)
      if (!res.ok)
      {
        throw new Error('Не удалось удалить запись...')
      }
      document.querySelector(`li[data-id="${id}"]`).remove()
    }
    catch (error)
    {
      this.showError(error.message)
    }
  },
}
